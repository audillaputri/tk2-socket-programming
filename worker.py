import asyncio
import sys

if len(sys.argv) < 2:
    print("Error: the number of argument is not enough")
    sys.exit()

HOST = str(sys.argv[1])
PORT = str(sys.argv[2])


async def worker_main(reader, writer):
    # read byte data
    byte_data = await reader.read(100)
    master_addr = writer.get_extra_info("peername")
    message = byte_data.decode()

    print(f"Received {message!r} from {master_addr!r}")

    processed_message = message.upper()

    await asyncio.sleep(2)

    writer.write(processed_message.encode())
    await writer.drain()
    writer.close()


async def worker_entrypoint(host, port):
    print(f"starting worker at {host}:{port}")
    worker = await asyncio.start_server(worker_main, host, port)
    async with worker:
        await worker.serve_forever()


asyncio.run(worker_entrypoint(HOST, PORT))
