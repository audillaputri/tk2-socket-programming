import sys
import asyncio


if len(sys.argv) < 4:
    print("error argument")
    sys.exit()

HOST = sys.argv[1]
PORT = sys.argv[2]
WORKER_HOST = sys.argv[3]

workers = [
    {"host": WORKER_HOST, "port": 5051, "total_conn": 0},
    {"host": WORKER_HOST, "port": 5052, "total_conn": 0},
    {"host": WORKER_HOST, "port": 5053, "total_conn": 0},
]


def get_available_worker():
    min_total_conn_worker = float("inf")
    available_worker = None
    for worker in workers:
        if worker["total_conn"] < min_total_conn_worker:
            available_worker = worker
            min_total_conn_worker = worker["total_conn"]

    available_worker["total_conn"] += 1
    return available_worker


def print_worker_status():
    for worker in workers:
        if worker["total_conn"] > 0:
            print(
                f"worker with port: {worker['port']} is RUNNING with total connection {worker['total_conn']}"
            )
        else:
            print(f"worker with port: {worker['port']} is available")


async def call_worker(message, worker, first_run):

    if first_run:
        print_worker_status()

    reader, writer = await asyncio.open_connection(worker["host"], worker["port"])

    print(f"=== send: {message!r} to worker with port: {worker['port']}")
    writer.write(message.encode())

    data = await reader.read(100)
    print(f"=== received: {data.decode()!r} from worker with port {worker['port']}")

    writer.close()

    worker["total_conn"] -= 1
    return data


async def main(messages):

    first_run = True
    res = []
    for message in messages:
        worker = get_available_worker()
        res.append(loop.create_task(call_worker(message, worker, first_run)))
        if first_run:
            first_run = False

    await asyncio.wait(res)

    print("done main")
    print_worker_status()

    return res


if __name__ == "__main__":
    banner = """
choose command:
1. input
2. exit
>>> """
    loop = asyncio.get_event_loop()
    while True:
        choice = input(banner)
        if int(choice) == 1:
            try:
                messages = input("input string (comma seperated): ").split(",")
                loop.run_until_complete(main(messages))
            except Exception as e:
                print(e)

        elif int(choice) == 2:
            loop.close()
            sys.exit()

        else:
            print("invalid command!")
